#! /bin/bash
WORK_DIR=/home/thanhpt/project/learn/cicd/buildCicd/builds/democicd
APP_PATH=target/democicd-0.0.1-SNAPSHOT.jar
pid_file=service.pid

RUNNING=0
if [ -f $pid_file ]; then
	pid=`cat $pid_file`
	if [ "x$pid" != "x" ] && kill -0 $pid 2>/dev/null; then
		RUNNING=1
	fi
fi

if [ -n "$JAVA_HOME" ]
then
  java_cmd="$JAVA_HOME/bin/java"
else
  java_cmd="$(which java)"
fi

start()

{
	cd $WORK_DIR
	if [ $RUNNING -eq 1 ]; then
		echo "Service already started"
	else
		
		$java_cmd -jar $APP_PATH  &
		echo $! > $pid_file
		echo "Service started"
	fi
}

stop()
{
	if [ $RUNNING -eq 1 ]; then
		kill -9 $pid
		echo "Service stopped"
	else
		echo "Service not running"
	fi
}

restart()
{
	stop
	start
}


case "$1" in

	'start')
		start
		;;

	'stop')
		stop
		;;

	'restart')
		restart
		;;

	*)
		echo "Usage: $0 {  start | stop | restart  }"
		exit 1
		;;
esac

exit 0
